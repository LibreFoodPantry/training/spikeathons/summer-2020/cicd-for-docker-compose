# Purpose
This project is an example of how to implement Gitlab's CI / CD in a project that uses Docker Compose.

# Usefulness
CI / CD allows for code to be automatically tested and deployed whenever a change is committed. This creates a more stable and reliable master branch, and it eliminates the need to manually deploy every commit.

# .gitlab-ci.yml
Gitlab's CI / CD pipeline is initatiated and configured by a .gitlab-ci.yml file. This file is a list of jobs that will be picked up by runners and whose behavior is defined by a set of parameters. The full reference document for this file can be found [here](https://docs.gitlab.com/ee/ci/yaml/README.html).

**Jobs** are the set of parameters to be executed by the runner. There is no limmit on how many jobs may exist within a pipeline, but each job must contain a "script" clause. A job's name is user-defined and arbitrary, but it should ideally describe the job's purpose. This project uses one job called "build".

**Stages** are used to designate jobs at the same level. Jobs within the same stage will run parallel to eachother, and stages run in the order they are specified. "Build", "test", and "deploy" are the default stages when nothing else is defined. This project does not specify any stages, and it does not specify its "build" job as part of any of the default stages. A better example of the use of stages in a project can be found [here](https://docs.gitlab.com/ee/ci/yaml/README.html#stages).

**Image** specifies the Docker image that the job will run on. If no image is specified, ruby:2.5 is used. This project uses docker/compose:latest.

**Services** specifies additional Docker images. These are linked to the base image specified in the "image" clause. This project uses docker:dind.

**Default** is used to specify parameters that will be used by every job. The parameters that can be set by default are as follows: image, services, before_script, after_script, tags, cache, artifacts, retry, timeout, interruptible. A job can override a pipeline's defaults by setting the parameter it intends to override within the job. This project does not utilize this feature; a better example can be found [here](https://docs.gitlab.com/ee/ci/yaml/#global-defaults).

**Script** is a shell script executed when a job is run. Jobs require a script to be valid. This project's job, "build", runs the commands "docker-compose up" and "docker-compose down".

**Before_Script & After_Script** define commands to run before and after a script respectively. Commands in before_script are run in the same shell as script, however commands in after_script are run in a seperate shell. In the commented out portion of this project's .gitlab-ci.yml file, before_script is used to run the command "apk add --no-cache docker-compose" and install docker-compose. This project does not utilize the after_script feature; a better example can be found [here](https://docs.gitlab.com/ee/ci/yaml/#before_script-and-after_script)

# Docker and Docker-Compose Specific
As with the use of Docker, the file requires special configuration in order to test Docker Compose. Either Docker Compose must be downloaded before attempting to use any docker-compose commands (there are many ways to accomplish this, but one example can be found in the commented out sections of this project's .gitlab-ci.yml file), or an image of Docker must be used that already has Compose installed (this project uses docker/compose:latest). The docker-in-docker service (docker:dind) must also be included in the .gitlab-ci.yml file so that the Runner is able to use Docker. 

When writing the script to test the Compose containers, Compose must be run detatched (docker-compose up -d) so that the Runner can continue to accept script commands while it is running. This will allow the Runner to accept commands like "docker-compose down" so that the containers are not infinitely running.